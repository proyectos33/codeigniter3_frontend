<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {


	public function index()
	{
		$data = array(
			'head' => $this->load->view('layouts/head','', true),
			'nav' => $this->load->view('layouts/nav','', true),
			'footer' => $this->load->view('layouts/footer','', true),
		);
		$this->load->view('inicio', $data);
	}
}
