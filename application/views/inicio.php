<!-- Encabezado -->
<?= $head ?>

	<!-- Barra de Navegacion -->
<?= $nav ?>



<div class="caja bg-dark">
	<img class="img-left" src="assets/img/Grupo_superior.png" alt="Grupo_superior">
	<img class="img-center" src="assets/img/Nixtamal.png" alt="elote_preview">
	<img class="img-right" src="assets/img/elote_preview.png" alt="elote_preview">
</div>

<div class="container">
	<div class="row">
		<div class="col-lg-5 mx-auto text-center py-4">
			<h1 class="text-body poppins display-4">Una nueva</h1>
			<h2 class="text-body">experiencia culinaria</h2>
			<p class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			<p class="text-secondary">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-lg-3">
			<div class="card mt-4 mb-3 mx-auto shadow-sm rounded">
				<img
					src="assets/img/fried.jpg"
					class="card-img-top img-fluid"
					alt="gastro"
				>
			</div>
		</div>

		<div class="col-12 col-lg-3">
			<div class="card mt-4 mb-3 mx-auto shadow-sm">
				<img
					src="assets/img/gastro.jpg"
					class="card-img-top img-fluid rounded"
					alt="picante"
				>
			</div>
		</div>

		<div class="col-12 col-lg-3">
			<div class="card mt-4 mb-3 mx-auto shadow-sm">
				<img
					src="assets/img/mongolianbeef.jpg"
					class="card-img-top img-fluid rounded"
					alt="gastro"
				>
			</div>
		</div>

		<div class="col-12 col-lg-3">
			<div class="card mt-4 mb-3 mx-auto shadow-sm">
				<img
					src="assets/img/great.jpg"
					class="card-img-top img-fluid rounded"
					alt="great"
				>
			</div>
		</div>
	</div>

	<div class="row justify-content-between align-items-center py-4">
		<div class="col-12 col-lg-6 py-4">
			<h1 class="text-body">LOREM IMPSUN</h1>
			<h2 class="text-body">dolor sit amet lore</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
		</div>
		<div class="col-12 col-lg-6 py-4">
			<video
				class="img-fluid"
				controls
				poster="assets/img/gastro.jpg">
			</video>
		</div>
	</div>
</div>


	<div class="caja bg-dark">
		<img class="img-left" src="assets/img/Grupo_superior.png" alt="Grupo_superior">
			<div class="container text-white">
				<div class="row">
					<div class="col-12 text-center py-4">
						<h1 class="baskerville display-4">SUSCRIPCIONES</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 justify-content-start d-flex flex-column">

						<a class="nav-link text-white" href="">
							Ver Video
							<img class="pl-2" src="assets/iconos/flecha_preview.png" alt="next">
						</a>
						<h2>BASICA</h2>
						<p class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
						<h2>Q000.000</h2>
						<div class="btn-group">
							<a class="btn btn-comprar rounded-pill">COMPRAR</a>
						</div>
					</div>
					<div class="col-lg-6 justify-content-start d-flex flex-column">

							<a class="nav-link text-white" href="">
								Ver Video
								<img class="pl-2" src="assets/iconos/flecha_preview.png" alt="next">
							</a>

						<h2>PREMIUM</h2>
						<p class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
						<h2>Q000.000</h2>
						<div class="btn-group">
							<a class="btn btn-comprar rounded-pill">COMPRAR</a>
						</div>
					</div>
				</div>
			</div>
		<img class="img-right" src="assets/img/Grupo_inferior.png" alt="Grupo_inferior">
	</div>


<!-- footer -->
<?= $footer ?>


