<footer style="color: white;" class="color-pink py-4 pb-0">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-3">
				<img  class="img-fluid" src="assets/img/grupo_claro.png" alt="Mr menu">
			</div>
			<div class="col-12 col-lg-3">
				<ul class="list-group">
			    <li class="list-group-item color-pink remarcado pl-0 barra-footer font-weight-bolder">MAS SOBRE MISTER MENU</li>
			    <li class="list-group-item color-pink border-0 pl-0">Anunciate con nosotros</li>
			    <li class="list-group-item color-pink border-0 pl-0">Quienes Somos</li>
			    <li class="list-group-item color-pink border-0 pl-0">Preguntas Frecuentes</li>
			    <li class="list-group-item color-pink border-0 pl-0">Legal</li>
				</ul>
			</div>
			<div class="col-12 col-lg-3">
				<ul class="list-group">
					<li class="list-group-item color-pink remarcado barra-footer pl-0 font-weight-bolder">QUE BUSCAS</li>
					<li class="list-group-item color-pink border-0 pl-0">Contenido</li>
					<li class="list-group-item color-pink border-0 pl-0">Experiencia</li>
					<li class="list-group-item color-pink border-0 pl-0">Restaurante</li>
					<li class="list-group-item color-pink border-0 pl-0">Mercadito</li>
				</ul>
			</div>
			<div class="col-12 col-lg-3">
				<ul class="list-group">
					<li class="list-group-item color-pink barra-footer pl-0 font-weight-bolder">ENCUENTRANOS</li>
				</ul>

				<div class="row">
					<div style="height: 25vh;" class="col-12 d-flex flex-column justify-content-between py-3">

						<div>
							<img class="border-1 border border-white rounded-pill p-2" src="assets/iconos/instagram.png" alt="instagram">
							<img class="border-1 border border-white rounded-pill" src="assets/iconos/fb.png" alt="fb">
							<img class="border-1 border border-white rounded-pill p-2" src="assets/iconos/youtube.png" alt="youtube">
						</div>

						<div class="btn-group">
						<a
							class=" rounded-pill p-2 border-1 border border-white text-white nav-link dropdown-toggle"
							href="#" id="navbarDropdown"
							role="button"
							data-toggle="dropdown"
							aria-haspopup="true"
							aria-expanded="false"
						>ES
				    </a>
				    </div>

					</div>
				</div>

			</div>
		</div>
	</div>

</footer>
<div class="pie-pagina text-white text-center baskerville">
	<h4 class="py-2">2020 Todos los Derechos Reservados. Mister Menu by SMT</h4>
</div>

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

	</body>
</html>