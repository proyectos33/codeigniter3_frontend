<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Codeigniter</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

	<link rel="icon" href="assets/iconos/grupo.ico">

	<style>
		.baskerville{
			font-family: Baskerville, serif;
		}
		.poppins{
			font-family: poppins, sans-serif;
		}

		.color-pink{
			background: #f0533f;
		}
		.inputWithIcon input[type="text"]{
			width: 25rem;
		}
		.inputWithIcon{
			position: relative;
		}

		.inputWithIcon img{
			position: absolute;
			bottom: 2px;
			right: 5px;
			padding: 6px 9px;
			background: red;
		}

		.barra-navegacion {
			border-bottom: 3px solid red;
		}

		.barra-footer{

			border-top:0px;
			border-left: 0px;
			border-right: 0px;
			border-bottom: 3px solid white;
		}

		.caja{
			position: relative;
			height: 500px;

		}
		.caja img[class="img-left"]{
			position: absolute;
			top: 0px;
			left: 0px;
		}
		.caja img[class="img-right"]{
			position: absolute;
			bottom: 0px;
			right: 0px;
		}
		.caja img[class="img-center"]{
			position: absolute;
			left: 250px;
			top: 180px;
		}

		.btn-comprar{
			background-color: #c4c4a5;
			color: white;

		}
		.nav-link{
			padding-left: 0;
		}

		.pie-pagina{
			background: #fc7665;
			height: 50px;
		}

		.btn-group{
			width: 25%;
		}








	</style>

</head>
<body>