
<nav class="navbar navbar-light navbar-expand-lg bg-white pl-5 px-5 barra-navegacion">

	<a href="#" class="navbar-brand pl-4">
		<img class="img-fluid" src="assets/img/grupo.jpg" alt="">
	</a>

	<label class="inputWithIcon pl-5">
		<input class="form-control rounded-pill" type="text" name="search" placeholder="Que buscas?">
			<img class="rounded-pill" src="assets/iconos/search.png" alt="search">
	</label>

	<button class="navbar-toggler" type="button"
    data-toggle="collapse"
    data-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent"
    aria-expanded="false"
    aria-label="{{ __('Toggle navigation') }}">
    <span class="navbar-toggler-icon"></span>
  </button>


	  <div class="collapse navbar-collapse" id="navbarSupportedContent">

		<ul class="nav ml-auto">
	    <li class="nav-item">
	    	<a class="nav-link text-body" href="">Nixtamal</a>
	    </li>
	    <li class="nav-item">
	    	<a class="nav-link text-body" href="#">Busqueda</a>
	    </li>
	    <li class="nav-item">
	    	<a class="nav-link text-body" href="#">Registrate</a>
	    </li>
	    <li class="nav-item">
	    	<a class="nav-link text-body" href="#">Login</a>
	    </li>
	    <li class="nav-item">
	    	<a class="nav-link text-body d-flex" href="#">
	    		<img class="mr-1" src="assets/iconos/location.png" alt="location">
	    		<span>Guatemala</span>
	    	</a>
	    </li>
		</ul>
	</div>

</nav>